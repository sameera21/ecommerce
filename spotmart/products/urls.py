
from django.conf.urls import url,include
from django.urls import path
from rest_framework import routers
from .api_views import ProductViewSet,OrderViewSet


router = routers.DefaultRouter()
router.register('rest_api/product',ProductViewSet,'products')
router.register('rest_api/order',OrderViewSet,'products')



urlpatterns = [
     path('', include(router.urls)), # add .urls after app name
]
