from django.contrib.auth.models import User
from rest_framework import viewsets
from products.models import Product,Order
from products.serializers import ProductSerializer,OrderSerializer


class ProductViewSet(viewsets.ModelViewSet):
     queryset = Product.objects.all()
     serializer_class = ProductSerializer


class OrderViewSet(viewsets.ModelViewSet):
      queryset = Order.objects.all()
      serializer_class= OrderSerializer
