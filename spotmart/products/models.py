
# from telnetlib import STATUS
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Product(models.Model):
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=300)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def _str_(self):
        return self.title


class Order(models.Model):
    PAYMENT_CHOICES=(
        ('C','Cash'),
        ('PTM','Paytm'),
        ('CRD','Card'),
    )

   
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    total = models.IntegerField()
    created_on= models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=30)
    mode_of_payments = models.CharField(max_length=30,choices=PAYMENT_CHOICES)

    def _str_(self):
        return str(self.id)


class OrderItems(models.Model):
    order_id = models.ForeignKey(Order, on_delete=models.CASCADE)
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=10, decimal_places=2)

    def _str_(self):
        return str(self.id)





    
    
